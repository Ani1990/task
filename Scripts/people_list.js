﻿$(function () {
    $("#sortable").sortable();
    $("#sortable").disableSelection();
});

$(document).ready(function () {
    $('#btn').click(function () {


        var back = [ "#F6CECE", "#F7F2E0", "#E3F6CE", "#CEF6F5", "#A9E2F3", "#F5A9BC","#F2F2F2"];
        var rand = back[Math.floor(Math.random() * back.length)];



        var text = $('#name').val()
        $("#sortable").append('<li style="background:' + rand + '">' + text + '<i>✖</i></li>');
        $('#name').val("")
    });
});